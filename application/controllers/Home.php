<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$this->load->view('header_view');
		$this->load->view('home/home_slider');
		$this->load->view('home/home_services');
		$this->load->view('home/home_doctors');
		$this->load->view('home/home_testimonials');
		$this->load->view('footer_view');
	}
}
