<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	function __construct()
    {
        parent::__construct();
    }
    
	public function index()
	{
		$this->load->view('header_view');
		$this->load->view('about/about_view');
		$this->load->view('footer_view');
	}
}
