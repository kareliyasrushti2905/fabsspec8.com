<!-- Revolution Slider Begins -->
    <div class="rev_slider_wrapper fullwidthbanner-container">
        <div id="slider-2" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <!-- Slides -->
            <ul>

                <!-- Slide 1 -->
                <li data-index="rs-3070" data-transition="parallaxhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off">

                    <!-- Main Image -->
                    <img src="<?php echo base_url() ?>assets/images/slider-4.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

                </li>

                <!-- Slide 2 -->
                <li data-index="rs-3071" data-transition="parallaxhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0"  data-saveperformance="off">

                    <!-- Main Image -->
                    <img src="<?php echo base_url() ?>assets/images/slider-3.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>

                </li>
            </ul>

            <!-- Static Text Layers-->
            <div class="tp-static-layers">

                <!-- Layer 1 -->
                <div class="tp-caption tp-resizeme tp-static-layer" 
                data-x="['left','left','left','left']" data-hoffset="['25','25','25','25']" 
                data-y="['center','center','center','center']" data-voffset="['-80','-80','-80','-80']" 
                data-fontsize="['50','45','40','30']"
                data-lineheight="['60','60','55','45']"
                data-whitespace="nowrap"
                data-type="text" 
                data-responsive_offset="on" 
                data-startslide="0" 
                data-endslide="3" 
                data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                data-textAlign="['left','left','left','left']"
                style="z-index: 5; white-space: nowrap;text-transform:left;"><h1>Word Class Healthcare <br/> for Your Pets.</h1></div>

                <!-- Layer 2 -->
                <div class="tp-caption tp-resizeme tp-static-layer"  data-x="['left','left','left','left']" data-hoffset="['25','25','25','25']"  data-y="['center','center','center','center']" data-voffset="['10','10','10','10']"  data-fontsize="['20','20','20','18']" data-lineheight="['30','30','30','20']" data-whitespace="nowrap" data-type="text"  data-responsive_offset="on"  data-startslide="0"  data-endslide="3"  data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                data-textAlign="['left','left','left','left']" style="z-index: 6; white-space: nowrap;text-transform:left;">Top class equipments & quality services. </div>

                <!-- Layer 3 -->
                <div class="tp-caption tp-resizeme btn btn-primary tp-static-layer"data-x="['left','left','left','left']"data-hoffset="['25','25','25','25']" data-y="['center','center','center','center']"data-voffset="['90','90','90','90']" data-width="none"data-height="none"data-whitespace="nowrap"data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'data-responsive_offset="on" data-responsive="off"data-startslide="0" data-endslide="3" data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'data-textAlign="['left','left','left','left']" style="z-index: 7; white-space: nowrap;cursor:pointer;">Know More</div>
                
            </div>
        </div>
    </div>
    <!-- Revolution Slider Ends -->