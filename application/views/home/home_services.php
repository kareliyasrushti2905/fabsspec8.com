<div class="container mt-80">
        <div class="row">
            <div class="col-sm-12">
                <div class="heading-block">
                    <h2 class="heading">Our <span class="color1">Quality</span> Services</h2>
                    <p class="sub-heading">Behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="feature-box left">
                    <span class="count">1</span>
                    <div class="text">
                        <h4 class="heading">Physiotherapy</h4>
                        <p>Physiotherapy is an area of medicine that deals with physical ailments that restore a person’s function and body performance, regardless of activity level.</p>
                    </div>
                </div>
                <div class="feature-box left">
                    <span class="count">2</span>
                    <div class="text">
                        <h4 class="heading">Sports Medicine</h4>
                        <p>You can see a Sports Medicine Physician who is devoted to assessing, diagnosing and providing medical management for musculoskeletal conditions.</p>
                    </div>
                </div>
                
            </div>
            <div class="col-sm-4">
                <img src="assets/images/service-img.jpeg" class="img-responsive mt-20" alt="">
            </div>
            <div class="col-sm-4">
                <div class="feature-box right">
                    <span class="count">3</span>
                    <div class="text">
                        <h4 class="heading">Pilates</h4>
                        <p>Pilates that improves muscle balance, alignment and mind-body connection which leads to pain free movement and better performance.</p>
                    </div>
                </div>
                <div class="feature-box right">
                    <span class="count">4</span>
                    <div class="text">
                        <h4 class="heading">Massage Therapy</h4>
                        <p>massage therapy providers are trained to use hands on manipulation techniques to resolve injuries, reduce tension and relieve pain.</p>
                    </div>
                </div>
            
            </div>
        </div>
    </div>