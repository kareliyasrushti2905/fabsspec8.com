<div class="container mt-80">
        <div class="row">
            <div class="col-sm-12">
                <div class="heading-block">
                    <h2 class="heading">Our <span class="color1">Speciality</span> Doctors</h2>
                    <p class="sub-heading">Behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="doctor-box-2">
                    <div class="doctor-img">    
                        <img src="assets/images/vaibhav.jpeg" class="img-circle" alt="" style="height:222px;width: 222px;">
                    </div>
                    <div class="doctor-name">
                        <h4 class="heading">Dr. Vaibhav Dave</h4>
                        <span class="doctor-desig">Director</span>
                    </div>
                    <br>
                    <!--<div class="text-center">
                        <a href="book-appointment-form.html" class="btn btn-primary btn-sm">Booking</a>
                        <a href="doctor-single.html" class="btn btn-black btn-sm">Profile</a>
                    </div>-->
                </div>
            </div>
            <div class="col-sm-4">
                <div class="doctor-box-2">
                    <div class="doctor-img">    
                        <img src="assets/images/prachi.jpeg" class="img-circle" alt="" style="height:222px;width: 222px;">
                    </div>
                    <div class="doctor-name">
                        <h4 class="heading">Dr. Prachi Oza</h4>
                        <span class="doctor-desig">Head Of Department (Rajkot)</span>
                    </div>
                    <br>
                    
                </div>
            </div>
            <div class="col-sm-4">
                <div class="doctor-box-2">
                    <div class="doctor-img">    
                        <img src="assets/images/vrajesh.jpeg" class="img-circle" alt="" style="height:222px;width: 222px;">
                    </div>
                    <div class="doctor-name">
                        <h4 class="heading">Dr. Vrajesh Thakkar</h4>
                        <span class="doctor-desig">Head Of Department (Gandhinagar)</span>
                    </div>
                    <br>
                    
                </div>
            </div>  
        </div>
    </div>