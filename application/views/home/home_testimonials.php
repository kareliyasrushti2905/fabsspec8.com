<div class="container mt-80">
    <div class="row">
        <div class="col-sm-12">
            <div class="heading-block">
                <h2 class="heading">People <span class="color1">Love</span> Us</h2>
                <p class="sub-heading">Behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
            </div>               
        </div>
        <div class="col-sm-12">
            <div class="testimonial-slider">
                <div class="slider-items">
                    <div>
                        <div class="testi-img">
                            <img src="assets/images/testimonial-1.jpeg" class="img-circle" alt="" style="height:100px;width:100px">
                        </div>
                        <br>
                        <p class="testi-heading">I cannot tell you how beautiful it is to see again...</p>
                        <p class="testi-text">Behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics. Behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                    </div>
                    <div>
                        <div class="testi-img">
                            <img src="assets/images/testimonial-2.jpeg" class="img-circle" alt="" style="height:100px;width:100px">
                        </div>
                        <br>
                        <p class="testi-heading">I cannot tell you how beautiful it is to see again...</p>
                        <p class="testi-text">Behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics. Behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                    </div>
                </div>
                <div class="slider-nav">
                    <div class="left"><i class="fa fa-angle-left"></i></div>
                    <div class="right"><i class="fa fa-angle-right"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>