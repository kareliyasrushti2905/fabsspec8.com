<div class="page-header">
        <div class="container">
            <h1>Contact Us</h1>
            <div class="breadcrumb-2">
                <ul class="breadcrumb">
                    <li><a href="index-hospital.html">Home</a></li>
                    <li class="active">Contact Us</li>
                </ul>
            </div>
        </div>
    </div>
<div class="container mt-60">
        <div class="row">
            <div class="col-sm-4 mt-20">
                <div class="p40 bgcolor2">
                    <img src="images/logo-dark.png" class="img-responsive" alt="">
                    <br/>
                    <p class="justify">Behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p>
                    <p class="justify nom">A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                </div>
            </div>
            <div class="col-sm-8 mt-20">
            	<div class="table-responsive">
                   <table class="table table-bordered table-schedule-mini">
                       <tbody>
                           <tr>
                               <th>Doctor</th>
                               <th>Name</th>
                               <th>Speciality</th>
                               <th>Email</th>
                           </tr>
                           <tr>
                               <td class="text-center">
                                <img src="<?php echo base_url();?>assets/images/vaibhav.jpeg" class="img-circle" alt="" style="height:60px;width:60px"></td>
                                <td><strong><a href="doctor-single-sidebar.html">Mr. Vaibhav Dave</a></strong></td>
                                <td>Neuro Care</td>
                                <td>vaibhavthephysio94@gmail.com</td>
                            </tr>
                            <tr>
                               <td class="text-center">
                                <img src="<?php echo base_url();?>assets/images/prachi.jpeg" class="img-circle" alt="" style="height:60px;width:60px"></td>
                                <td><strong><a href="doctor-single-sidebar.html">Ms. Prachi Oza</a></strong></td>
                                <td>Psychotherapy</td>
                                <td>prachi000196@gmail.com</td>
                            </tr>
                            <tr>
                               <td class="text-center">
                                <img src="<?php echo base_url();?>assets/images/vrajesh.jpeg" class="img-circle" alt="" style="height:60px;width:60px"></td>
                                <td><strong><a href="doctor-single-sidebar.html">Mr. Vrajesh Thakkar</a></strong></td>
                                <td>Heart Specialist</td>
                                <td>Dr.Vrajeshthakkar@gmail.com</td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<div class="container mt-80 ">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="heading">Contact Us. <span class="color1">Drop us a line!</span></h3>
                <form id="main-contact-form">
                 <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Your First Name" id="fname" name="fname">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Your Last Name" id="lname" name="lname">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Your Email Id" id="email" name="email">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Your Subject Line" id="subject" name="subject">
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="5" placeholder="Please enter your message" id="message" name="message"></textarea>
                </div>
                <p id="status"></p>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit" name="submit"><i class="fa fa-paper-plane"></i> Submit Query</button>
                </div>
            </form>
        </div>
    </div>
</div>
