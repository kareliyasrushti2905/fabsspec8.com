<div class="page-header">
        <div class="container">
            <h1>Gallery</h1>
            <div class="breadcrumb-2">
                <ul class="breadcrumb">
                    <li><a href="index-hospital.html">Home</a></li>
                    <li class="active">Gallery</li>
                </ul>
            </div>
        </div>
</div>
<div class="container mt-80 mb-60">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="heading text-center">Patient Care At FabsSpec</h2>
                        <hr class="hr-1">
                    </div>
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/11.jpeg" target="_blank" data-gal="prettyPhoto[galleryName]" class="img-open" target="_blank"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/11.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/2.jpeg" target="_blank" data-gal="prettyPhoto[galleryName]" class="img-open" target="_blank"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/2.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/8.jpeg" target="_blank" data-gal="prettyPhoto[galleryName]" class="img-open" target="_blank"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/8.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/4.jpeg" target="_blank" data-gal="prettyPhoto[galleryName]" class="img-open"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/4.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/5.jpeg" target="_blank" data-gal="prettyPhoto[galleryName]"  class="img-open"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/5.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/19.jpeg" target="_blank" data-gal="prettyPhoto[galleryName]" class="img-open" target="_blank"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/19.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                </div>
                <div class="row mt-80">
                    <div class="col-sm-12">
                        <h2 class="heading text-center">National Tournaments</h2>
                        <hr class="hr-1">
                    </div>
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/18.jpeg" target="_blank" data-gal="prettyPhoto[galleryName]" class="img-open" target="_blank"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/18.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/15.jpeg" target="_blank" data-gal="prettyPhoto[galleryName]" class="img-open" target="_blank"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/15.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/7.jpeg" target="_blank" data-gal="prettyPhoto[galleryName]"  class="img-open"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/7.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/6.jpeg" data-gal="prettyPhoto[galleryName]" target="_blank" class="img-open"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/6.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="img-box">
                            <a href="<?php echo base_url();?>assets/images/14.jpeg" data-gal="prettyPhoto[galleryName]"  class="img-open" target="_blank"><i class="fa fa-search-plus"></i></a>
                            <img src="<?php echo base_url();?>assets/images/14.jpeg" class="img-responsive" alt="" style="height:250px;width:350px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

