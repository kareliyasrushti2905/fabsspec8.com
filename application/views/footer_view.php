<footer class="footer-1">           <!-- Footer Style 1 -->
        <div class="footer-pri pt-20 pb-40">            <!-- Primary Footer -->
            <div class="container">
            
                <div class="row">
                    <div class="col-sm-4">
                        <div class="widget widget-about">
                            
                               <hr/>
                               <ul class="contact">
                                <h3 class="color1"> Dr. Vaibhav Dave </h3>
                                <li><i class="fa fa-phone"></i> +91 96360 33183 </li>
                                <li><i class="fa fa-envelope"></i> vaibhavthephysio94@gmail.com </li>
                            </ul>
                            <hr/>
                            <!--<ul class="social social-2x">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                            </ul>-->
                          
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="widget widget-about">
                            
                               <hr/>
                               <ul class="contact">
                                <h3 class="color1"> Dr. Prachi Oza </h3>
                                <li><i class="fa fa-phone"></i> +91 96603 68255 </li>
                                <li><i class="fa fa-envelope"></i> prachi000196@gmail.com </li>
                            </ul>
                            <hr/>
                            <!--<ul class="social social-2x">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                            </ul>-->
                            
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="widget widget-about">
                        
                               <hr/>
                               <ul class="contact">
                                <h3 class="color1"> Dr. Vrajesh Thakkar  </h3>
                                <li><i class="fa fa-phone"></i> +91 77279 67620 </li>
                                <li><i class="fa fa-envelope"></i> Dr.Vrajeshthakkar@gmail.com </li>
                            </ul>
                            <hr/>
                            <!--<ul class="social social-2x">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                            </ul>-->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-sec">            <!-- Secondary Footer -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <span>&copy; 2019 All Rights Reserved. <a href="#" target="_blank">www.fabsspec8.com</a></span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div id="back"><i class="fa fa-angle-up"></i></div>

    <!-- Modal -->
    

 <!-- Modal -->
 

<!-- JQuery Version 3.1.0 -->
<script src="<?php echo base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>

<!-- Bootstrap Version 3.3.7 -->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--Slider Revolution version 5.0-->
<script type="text/javascript" src="<?php echo base_url();?>assets/slider-revolution/revolution/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/slider-revolution/revolution/js/jquery.themepunch.revolution.min.js"></script>

<!-- Include only when working on Local system. Not required on server -->
<script type="text/javascript" src="<?php echo base_url();?>assets/slider-revolution/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/slider-revolution/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/slider-revolution/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/slider-revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/slider-revolution/revolution/js/extensions/revolution.extension.navigation.min.js"></script>

<!-- Bootstrap Select (Dropdown Styling) -->
<script src="<?php echo base_url();?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>

<!-- Owl Carousel 2.0.0 -->
<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js" type="text/javascript"></script>

<!-- Appear JS -->
<script src="<?php echo base_url();?>assets/js/jquery.appear.js" type="text/javascript"></script>

<!-- Count To JS -->
<script src="<?php echo base_url();?>assets/js/jquery.countTo.js" type="text/javascript"></script>

<!-- jQuery UI (Date Picker) -->
<script src="<?php echo base_url();?>assets/js/jquery-ui.min.js" type="text/javascript"></script>

<!-- Custom JS -->
<script src="<?php echo base_url();?>assets/js/script.js" type="text/javascript"></script>

</body>
</html>
