<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Fabsspec8.com</title>
    
    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon-32x32.png" type="image/x-icon" />
    
    <!--Bootstrap Framework Version 3.3.7--> 
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css" rel="stylesheet">
    
    <!--Font Awesome Version 4.6.3 --> 
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    
    <!-- Medical Icons -->
    <link href="<?php echo base_url();?>assets/css/medical-icons.css" type="text/css" rel="stylesheet">

    <!-- Stylesheets --> 
    <link href="<?php echo base_url();?>assets/css/vendors.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" type="text/css" rel="stylesheet" id="style">
    <link href="<?php echo base_url();?>assets/css/components.css" type="text/css" rel="stylesheet" id="components">

    <!-- Revolution Slider 5.4 -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/slider-revolution/revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/slider-revolution/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sliderrevolution/revolution/css/navigation.css">

    <!--Google Fonts--> 
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400,400i" rel="stylesheet">
    
    <!-- Respond.js and HTML shiv provide HTML5 support in older browsers like IE9 and 8 -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
     })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');

     ga('create', 'UA-79657248-2', 'auto');
     ga('send', 'pageview');
 </script>

</head>
<body>

    <div class="loader-backdrop">           <!-- Loader -->
        <div class="loader">
        </div>
    </div>
<header class="header-1">           <!-- Header Style 1 -->
        <div class="topbar">            <!-- Topbar -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="social">
                            <li><a href="https://www.facebook.com/FABSSPEC8/"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <ul class="contact">
                            <li><span>Email: </span> vaibhavthephysio94@gmail.com</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <nav class="navbar navbar-default">         <!-- Navigation Bar -->
            <div class="container">            
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false">
                        <span class="sr-only">Toggle Menu</span>
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url();?>home">
                        <img src="<?php echo base_url();?>assets/images/fabsspec-logo.png" alt="" style="height:110px;width:200px">         <!-- Replace with your Logo -->
                    </a>
                </div>
                
                <div class="collapse navbar-collapse" id="main-navigation">         <!-- Main Menu -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="<?php echo base_url();?>home">Home</a></li>
                        <li class="mega-sub-menu"><a href="<?php echo base_url();?>about">About Us</a></li>
                        <li class="mega-sub-menu"><a href="<?php echo base_url();?>gallary">Gallary</a></li>
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="<?php echo base_url();?>contact">Contact</a></li>
                        <li><a href="#">Appointment</a></li>
                    </ul>
                </div>
            </div>            
        </nav>
    </header>